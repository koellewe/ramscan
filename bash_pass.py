
"""Volatility plugin to extract passwords from bash commands"""
from bashlex.errors import ParsingError

import volatility.plugins.linux.common as common
import volatility.plugins.linux.bash as linux_bash
from bashlex import parser, ast

from volatility.renderers import TreeGrid

SUPPORTED_COMMANDS = ['zip', 'docker', 'ssh-keygen']


class CommandVisitor(ast.nodevisitor):
    def __init__(self, pwdlist):
        self.pwdlist = pwdlist
        # n-tuple: (program, file, password)

    def extract_option(self, command_parts, flags):
        """Extract a command-line option by flag. Returns index and value. -1 if not found"""
        item_index = -1
        for i, wordNode in enumerate(command_parts):  # iterate through command parts
            eqi = wordNode.word.find('=')
            if eqi > -1:  # --opt=val
                flag = wordNode.word[:eqi]
                if flag in flags:
                    return i, wordNode.word[eqi+1:]

            else:  # --opt val
                flag = wordNode.word
                if flag in flags:
                    if i == len(command_parts)-1:  # option is last in list
                        return -1, None
                    else:
                        return i+1, command_parts[i+1].word

        return -1, None

    def visitcommand(self, n, parts):
        # expecting all parts to be WordNodes
        if parts[0].word == 'zip':
            pwd_index, password = self.extract_option(parts, ['-P', '--password'])
            if pwd_index == -1:
                return  # dud command

            zipfile = '[Unknown]'
            # crudely expecting zip file to be first argument ending in .zip after password
            for wordNode in parts[pwd_index+1:]:
                if wordNode.word.endswith('.zip'):
                    zipfile = wordNode.word
                    break

            self.pwdlist.append(('zip', zipfile, password))

        elif parts[0].word == 'docker' and parts[1].word == 'login':
            pwd_index, password = self.extract_option(parts, ['-p', '--password'])
            if pwd_index == -1:
                return  # dud command

            usr_index, username = self.extract_option(parts, ['-u', '--username'])
            if username is None:
                username = '[Unknown]'

            self.pwdlist.append(('docker', username, password))

        elif parts[0].word == 'ssh-keygen':
            pwd_index, password = self.extract_option(parts, ['-P',])
            if pwd_index == -1:
                return  # dud command

            file_index, keyfile = self.extract_option(parts, ['-f',])
            if keyfile is None:
                keyfile = '[Unknown]'

            self.pwdlist.append(('ssh-keygen', keyfile, password))


# Snake case is the standard naming convention for volatility plugins
class linux_bash_pass(common.AbstractLinuxCommand):
    """Extract passwords entered into bash commands.

See https://gitlab.com/koellewe/ramscan/-/wikis/Linux_Bash_Pass for details"""

    def calculate(self):

        bash_data = linux_bash.linux_bash(self._config).calculate()

        for task_details, hist_item in bash_data:
            full_cmd = str(hist_item.line.dereference())
            cmd_time = hist_item.time_object()

            # check if command is of interest
            inspect_cmd = False
            for needle in SUPPORTED_COMMANDS:
                if full_cmd.find(needle) >= 0:  # list.index() throws an error if not found
                    inspect_cmd = True
                    break

            if inspect_cmd:
                try:
                    cmd_parts = parser.parse(full_cmd)
                except ParsingError:  # probably not a command we're interested in
                    continue

                parsed = []
                for ast in cmd_parts:
                    # visit all commands in tree, and extract passwords where applicable
                    CommandVisitor(parsed).visit(ast)

                    for cmd_data in parsed:
                        yield cmd_time, cmd_data

    def unified_output(self, data):
        """Standardised output"""
        return TreeGrid([("Time", str),
                         ("Program", str),
                         ("File/Username", str),
                         ("Password", str)],
                        self.generator(data))

    def generator(self, data):
        """Data -> unified output format converter"""
        for cmd_time, cmd_data in data:
            yield 0, [
                str(cmd_time),
                cmd_data[0],
                cmd_data[1],
                cmd_data[2],
            ]

    def render_text(self, outfd, data):
        self.table_header(outfd, [("Time", '32'),
                                  ("Program", '16'),
                                  ("File/Username", '22'),
                                  ("Password", '32')])

        for cmd_time, cmd_data in data:
            self.table_row(outfd, str(cmd_time), cmd_data[0], cmd_data[1], cmd_data[2])




